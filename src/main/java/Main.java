import lombok.SneakyThrows;

public class Main {
    @SneakyThrows
    public static void main(String[] args) {
        CoffeeMaker coffee = new CoffeeMaker();

        Thread thread = new Thread(coffee.add());
        Thread thread1 = new Thread(coffee.deliver());
        thread.start();
        thread1.start();
        thread.join();
        thread1.join();

        coffee.print();


    }

}
